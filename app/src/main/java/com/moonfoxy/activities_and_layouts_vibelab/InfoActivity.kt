package com.moonfoxy.activities_and_layouts_vibelab

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class InfoActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val textView: TextView = findViewById(R.id.textView)
        val textSurname: String = intent.extras!!.getString("text_surname")!!
        val textName: String = intent.extras!!.getString("text_name")!!
        val textPatronymic: String = intent.extras!!.getString("text_patronymic")!!
        val textAge: String = intent.extras!!.getString("text_age")!!
        val textHobby: String = intent.extras!!.getString("text_hobby")!!

        val dinosaurName = when(textAge.toInt().div(20)) {
            0 -> getString(R.string.dinosaur_name_1)
            1 -> getString(R.string.dinosaur_name_2)
            2 -> getString(R.string.dinosaur_name_3)
            3 -> getString(R.string.dinosaur_name_4)
            else -> getString(R.string.dinosaur_name_5)
        }

        textView.text = """
            Досье на $textSurname $textName $textPatronymic:
            Вид: $dinosaurName
            Год рождения: ${textAge}00 до н.э.
            Вид деятельности: $textHobby
        """.trimIndent()
    }
}