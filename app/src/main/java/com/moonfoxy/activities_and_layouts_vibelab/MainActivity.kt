package com.moonfoxy.activities_and_layouts_vibelab

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.ComponentActivity

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button: Button = findViewById(R.id.button)
        val editTextSurname: EditText = findViewById(R.id.editTextSurname)
        val editTextName: EditText = findViewById(R.id.editTextName)
        val editTextPatronymic: EditText = findViewById(R.id.editTextPatronymic)
        val editTextAge: EditText = findViewById(R.id.editTextAge)
        val editTextHobby: EditText = findViewById(R.id.editTextHobby)

        button.setOnClickListener {
            when {
                editTextSurname.text.toString() == "" -> {
                    Toast
                        .makeText(
                            this,
                            getString(R.string.toast_surname),
                            Toast.LENGTH_LONG
                        ).show()
                }
                editTextName.text.toString() == "" -> {
                    Toast
                        .makeText(
                            this,
                            getString(R.string.toast_name),
                            Toast.LENGTH_LONG
                        ).show()
                }
                editTextPatronymic.text.toString() == "" -> {
                    Toast
                        .makeText(
                            this,
                            getString(R.string.toast_patronymic),
                            Toast.LENGTH_LONG
                        ).show()
                }
                editTextAge.text.toString() == "" -> {
                    Toast
                        .makeText(
                            this,
                            getString(R.string.toast_age),
                            Toast.LENGTH_LONG
                        ).show()
                }
                editTextHobby.text.toString() == "" -> {
                    Toast
                        .makeText(
                            this,
                            getString(R.string.toast_hobby),
                            Toast.LENGTH_LONG
                        ).show()
                }
                else -> {
                    Intent(this, InfoActivity::class.java).also {
                        it.putExtra("text_surname", editTextSurname.text.toString())
                        it.putExtra("text_name", editTextName.text.toString())
                        it.putExtra("text_patronymic", editTextPatronymic.text.toString())
                        it.putExtra("text_age", editTextAge.text.toString())
                        it.putExtra("text_hobby", editTextHobby.text.toString())
                        startActivity(it)
                        finish()
                    }
                }
            }
        }
    }
}